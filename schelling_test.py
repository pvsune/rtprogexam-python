import unittest

from schelling import loadgrid, dissimilarity, schelling

class TestGridMethods(unittest.TestCase):

    def test_load_grid(self):
        self.assertEqual(
            ['XXOX XOOXX', 'O OXXOXOXO', 'OO OXO OXO'], \
            loadgrid('simple_grid.txt'))

    def test_load_invalid_grid(self):
        self.assertEqual(-1, loadgrid('invalid_grid.txt'))

    def test_grid_dissimilarity(self):
        self.assertEqual(0.05952, dissimilarity(
            loadgrid('simple_grid.txt'), 0, 0, 1, 3))

    def test_dissimilarity_invalid_subregion(self):
        self.assertEqual(0, dissimilarity(
            loadgrid('simple_grid.txt'), 0, 3, 1, 0))

    def test_dissimilarity_same_point_as_subregion(self):
        self.assertEqual(0.03571, dissimilarity(
            loadgrid('simple_grid.txt'), 2, 9, 2, 9 ))

    def test_dissimilarity_whole_grid_as_subregion(self):
        self.assertEqual(0, dissimilarity(
            loadgrid('simple_grid.txt'), 0, 0, 2, 9))

    def test_grid_schelling(self):
        self.assertEqual(['X  X', 'XXXO', '  OO'], schelling(
            loadgrid('mini_grid.txt'), 0.25))

    def test_grid_schelling_again(self):
        self.assertEqual(['X  X', 'XX O', '  OO'], schelling(
            loadgrid('mini_grid.txt'), 0.5))

    def test_schelling_invalid_t(self):
        self.assertEqual(None, schelling(loadgrid('mini_grid.txt'), 1.5))



if __name__ == '__main__':
    unittest.main()
