from __future__ import division


def loadgrid(grid_file):
    """Returns a grid data structure"""

    with open(grid_file) as f:
        grid = f.read().splitlines()

    # First row is the grid width.
    width = len(grid[0])

    for x in grid:
        if len(x) is not width:
            return -1

    return grid


def dissimilarity(grid, row1, col1, row2, col2):
    """Finds the index of dissimilarity of the grid or a subregion

    (row1, col1) should be upper-left corner of subregion,
    (row2, col2) should be lower-right
    """

    grid_c = __find_agent_count(grid, 0, 0, *__find_grid_dimensions(grid))
    subregion_c = __find_agent_count(grid, row1, col1, row2, col2)

    index = 0.5 \
        * abs(subregion_c[0]/grid_c[0] \
            - subregion_c[1]/grid_c[1])

    # Rounded to five (5) digits for precision.
    return round(index, 5)


def schelling(grid, t):
    """Transforms given grid based on Schelling's model"""

    if not 0 < t < 1: return
    newgrid = list()

    for key_row, row in enumerate(grid):
        newgrid_row = str()

        for key_agent, agent in enumerate(row):
            neighbors = __find_neighbors(grid, key_row, key_agent)

            for x in neighbors:
                if x is ' ': neighbors.remove(x)

            newgrid_row += agent \
                if neighbors.count(agent)/len(neighbors) >= t \
                else ' '

        newgrid.append(newgrid_row)

    return newgrid


def __find_neighbors(grid, key_row, key_agent):
    prev_row = key_row-1
    next_row = key_row+1
    prev_agent = key_agent-1
    next_agent = key_agent+1

    width = len(grid[0])-1
    length = len(grid)-1

    neighbors = list()

    # HACK: Trying out simple if statements in getting neighbors;
    #       see my Perl implementation.
    if prev_row >= 0:
        if prev_agent >= 0: neighbors.append(grid[prev_row][prev_agent])
        if next_agent <= width: neighbors.append(grid[prev_row][next_agent])
        neighbors.append(grid[prev_row][key_agent])

    if prev_agent >= 0: neighbors.append(grid[key_row][prev_agent])
    if next_agent <= width: neighbors.append(grid[key_row][next_agent])

    if next_row <= length:
        if prev_agent >= 0: neighbors.append(grid[next_row][prev_agent])
        if next_agent <= width: neighbors.append(grid[next_row][next_agent])
        neighbors.append(grid[next_row][key_agent])

    return neighbors

def __find_agent_count(grid, row1, col1, row2, col2):

    x_count = 0
    o_count = 0

    i = 0
    for row in grid:
        if i >= row1 and i <= row2:
            x_count += row.count('X', col1, col2+1)
            o_count += row.count('O', col1, col2+1)

        i += 1

    return [x_count, o_count]

def __find_grid_dimensions(grid):

    grid_l = len(grid) - 1
    grid_w  = len(grid[0]) - 1

    return [grid_l, grid_w]
